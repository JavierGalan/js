var mensaje = 'hola soy un mensaje';
// console.log(mensaje);

// console.log(typeof Array.prototype);
var name = 'javier';
var age = 30;
var alive = true;
var work = null;
var person = {name: 'Javier', age: 30, alive:true, work:null};
// console.log(person);
// console.log('Hola me llamo, ' + name + ' y tengo ' + age + ' años');
// operadores lögicos
var tengoEfectivo = true;
var tengoTarjeta = false;
var puedoPagar = tengoEfectivo || tengoEfectivo;
// console.log('puego pagar',puedoPagar);

var tengoCoche = true;
var tengoCarnet = false;
var puedoConducir = tengoCoche && tengoCarnet;
// console.log('puedo conducir');

// ejemplo
var tengoHambre = true;
var hayGazpacho = false;
var preparoGazpachito = true;
var comerGazpachito = tengoHambre && (hayGazpacho || preparoGazpachito);
console.log('Can I comer gazpacho?', comerGazpachito);

// crear y un objeto con nombre casa y las propiedades vasos(4) y latos(5) he imprimirlo en un console.log()
// con la frase En mi casa tengo 4 vasos y 5 platos.

var casa = {vasos:4, platos:5};
console.log(`En mi casa tengo ${casa.vasos} y ${casa['platos']}.`);