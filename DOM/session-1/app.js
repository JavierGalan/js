function sayHello(event) {
  alert("Acabas de pulsar el Botón");
  console.log(event);
}
function sayHelloJs() {
  alert("Desde Javascript");
  console.log(myWelcome);
  console.log(myWelcomeArray);
  console.log(myPatata);
}
document.getElementById("btnJs").addEventListener("click", sayHelloJs);
var myWelcome = document.querySelector(".welcome");
var myWelcomeArray = document.querySelectorAll(".welcome");
var myPatata = document.querySelector("#patata");
// ONLOAD -> SE LLAMA CUANDO SE CARGA LA VENTANA DEL NAVEGADOR
window.onload = function () {
  // Llmaa a nuestra función
  setTimeout(function () {
    addElementsJs();
  }, 2000);
  addRandomImages();
};
function addElementsJs() {
  // Crea un Div contenedor
  var newContainer = document.createElement("div");
  // Creamos una cadena de texto
  var newContent = document.createTextNode("Hola desde Js");
  // Añadimos la cadena sobre el Div contenedor
  newContainer.appendChild(newContent);
  // Recogemos la referencia del elemento del HTML
  var getDivFather = document.getElementById("createdJs");
  // Le indicamos el elemento que queremos añadir y su referencia en el Html
  document.body.insertBefore(newContainer, getDivFather);
}

var myImgs = [
  "https://cdn.pixabay.com/photo/2018/10/05/21/29/bat-3726896_960_720.png",
  "https://cdn.pixabay.com/photo/2020/07/06/17/51/batman-5377804_960_720.png",
];

function addRandomImages() {
  for (let i = 0; i < myImgs.length; i++) {
    var newContainerImage = document.createElement("div");
    var myImage = document.createElement("img");
    myImage.src = myImgs[i];
    newContainerImage.appendChild(myImage);
    document.body.insertAdjacentElement("beforeend", newContainerImage);
  }
}
