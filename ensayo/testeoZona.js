console.log('welcome to la testeo Zona');

const histTexts = [
  "La Alhambra se levantó sobre la colina de la Sabika, uno de los puntos más elevados de la ciudad de Granada. Este emplazamiento buscaba una situación estratégica defensiva y a la vez transmitir un claro símbolo, donde la cima del poder es muy perceptible para el resto de la ciudad, una ubicación escogida para ser contemplada.",
  "La colina de la Sabika ya era un espacio ocupado con anterioridad, al menos desde tiempo de los romanos y las primeras referencias escritas de un emplazamiento militar en la zona datan del año 666. Así se tiene constancia de los núcleos de Iliberis (Elvira), en el Albaicín y Alcazaba, Castilia, cerca del actual pueblo de Atarfe, y Garnata en la colina frente a la Alcazaba como un barrio de Iliberis en la comarca y en el 756 los núcleos de El Albaicín y La Alhambra. La extensión de la colina permite albergar el complejo ocupando este unos 740 m de longitud y entre 180 m y 40 m de anchura.",
  "A partir de la muerte de Alhakén II, tercer califa Omeya, en 976, la historia política del califato Omeya se convirtió en un cúmulo de reveses. La reconquista cristiana pasó a ser el impulso dominante dentro de la península. En este contexto, la transformación de Gharnata de pequeña población en ciudad de cierta importancia ocurrió a principios del siglo xi, cuando la dinastía bereber de los Ziríes formó un principado semiindependiente. Bajo los tres gobernantes Habus, Badis y Abdallah (1025-1090) la ciudad aumentó en población.",
  "Los edificios estaban concentrados en la colina de la Alcazaba y en su entorno inmediato. Para la formación de la Alhambra el acontecimiento más importante es la construcción por el visir Yusuf ibn Nagrela de una fortaleza-palacio en la colina de la Sabika. Un poema de Ibn Gabirol parece indicar que los leones de la fuente de los Leones se hallaban originariamente en el palacio de este visir judío. Puede que haya habido otras construcciones puramente militares en la colina de la Alhambra durante el período zirí, pero es difícil distinguirlas con precisión.",
  "El año 1238 señaló un segundo momento crucial. Muhammad ibn Nasr tomó la ciudad. La paradoja de la Granada nazarí consiste en el hecho de que un poder político y militar decadente, moribundo en realidad, coincidiera con una cultura original y sorprendentemente rica. La Alhambra se creó en un mundo políticamente inestable y económicamente próspero. Así, Muhammad aceptó una relación de vasallaje con la corona de Castilla, y así entró por la Puerta de Elvira para ocupar el palacio del Gallo del Viento (la antigua Alhambra), Mohamed-Ben-Nazar (o Nasr), llamado Al-Hamar el Rojo por el color de su barba.",
  "Ben-Al-Hamar construyó el primer núcleo del palacio, fortificándolo posteriormente su hijo Mohamed II. Con toda probabilidad las murallas exteriores y el acueducto se completaron al final del siglo xiii. Los jardines y pabellones del Generalife datan al parecer del reinado de Isma'il (1314-1325). Pero los emplazamientos más importantes de la Alhambra (el complejo del patio de los Arrayanes y el de los Leones) pertenecen a la época de Yusuf I (1333-1354) y Muhammed V, este estilo granadino es la culminación del arte andalusí. Tras tres siglos de actividad, quedan bien diferenciados los tres sectores en la Alhambra: la alcazaba, el entramado urbano y los palacios.",
  "En 1492, finalizó la conquista de Granada por los Reyes Católicos. Hernando del Pulgar, cronista de la época, cuenta: «El conde de Tendilla y el Comendador Mayor de León, Gutierre de Cárdenas, recibieron de Fernando el Católico las llaves de Granada, entraron en la Alhambra y encima de la Torre de Comares alzaron la cruz y la bandera». La Alhambra pasa así a ser ciudadela y palacio real de los reyes cristianos y el complejo continúa su desarrollo, se añade el convento de San Francisco en 1494, el palacio de Carlos V en 1527 o la iglesia de Santa María de la Encarnación de la Alhambra en 1581.",
];
// puertas
const arrAcessImg = [
  "https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Granada_2015_10_22_2348_%2825442616903%29.jpg/1280px-Granada_2015_10_22_2348_%2825442616903%29.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/3/3c/Puertas_de_las_armas_002.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/1/11/Puerta_de_la_Justicia%2C_o_Bab_al-Shari%27a_%28la_Alhambra%29.jpg/800px-Puerta_de_la_Justicia%2C_o_Bab_al-Shari%27a_%28la_Alhambra%29.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Puerta_del_Vino-Alhambra.jpg/800px-Puerta_del_Vino-Alhambra.jpg",
  "https://upload.wikimedia.org/wikipedia/commons/e/ec/Torre_de_los_siete_suelos.jpg",
];
// texts
const arrDoorHeading = [
  "Puerta de las Granadas",
  " Puerta de las Armas",
  "Puerta de la justicia",
  "Puerta del vino",
  "Puerta de los siete suelos",
];
// constructor
class htmlElement {
  constructor(blockType, id, classL, atributes, text) {
    this.blockType = blockType;
    this.classL = classL;
    this.id = id
    this.atributes = atributes;
    this.text = text;
  }
  createBlock() {
    const block = document.createElement(this.blockType);
    // introducir texto
    if (this.text != null) {
      const blockText = document.createTextNode(this.text);
      block.appendChild(blockText);
    }
    // atributos del elemento
    if (this.atributes != null) {
      for (const key in this.atributes) {
        block.setAttribute(key, `${this.atributes[key]}`);
      }
    }
    // classes del elemento
    for (let itemClass of this.classL) {
      block.classList.toggle(itemClass);
    }
    if (this.id != null) {
      block.id = this.id;
    }

    return block;
  }
}
const containerBlock = new htmlElement("div",null,["b-container"],null,null);

// container
const container = document.body.appendChild(containerBlock.createBlock());

//accesos
const doorContainer = new htmlElement("div", null, ["b-flex","b-flex__w80", "b-flex__row"],null,null);
const doorCard = new htmlElement("div","door-card", ["b-flex","b-flex__w20", "b-flex__column"],null,null);
const doorImg = new htmlElement("img",null, ["b-nav__img"],null,null);
const doorText = new htmlElement("h4",null,["b-text","b-text__para", "b-text__door"],null, null);


function createMultiple(father, object, arrText, arrLink) {
  arrText.forEach((item, item2) => {
    object.text = item;
    object.atributes = { href: arrLink[item2] };
    father.appendChild(object.createBlock());
  });
}   
// funcion para hacer la galeria
function galeryBuilder(father, child, grandChild1, img, text,arrImg,arrText) {
  const container = father.appendChild(child.createBlock());
  arrImg.forEach((title,indexTitle)=>{
    console.log(grandChild1);
    const elem1 = container.appendChild(grandChild1.createBlock());
    const elem2 = elem1.appendChild(img.createBlock());
    elem2.src = arrImg[indexTitle]
    const elem3 = elem1.appendChild(text.createBlock());
    elem3.innerHTML = arrText[indexTitle] 
  });
  
}
// const galery = container.appendChild(
//   galeryBuilder(
//     container,
//     doorContainer,
//     doorCard,
//     doorImg,
//     doorText,
//     arrAcessImg,
//     arrdoorHeading
//   )
// );
galeryBuilder(
  container,
  doorContainer,
  doorCard,
  doorImg,
  doorText,
  arrAcessImg,
  arrDoorHeading
);