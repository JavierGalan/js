// constructor
class htmlElement {
  constructor(blockType, classL, atributes, text) {
    this.blockType = blockType;
    this.classL = classL;
    this.atributes = atributes;
    this.text = text;
  }
  createBlock(blockType, classL, atributes, text) {
    const block = document.createElement(this.blockType);
    // introducir texto
    if (this.text.length > 0) {
      const blockText = document.createTextNode(this.text);
      block.appendChild(blockText);
    }
    // atributos del elemento
    if (this.atributes) {
      for (const key in this.atributes) {
        block.setAttribute(key, `${this.atributes[key]}`);
      }
    }
    // classes del elemento
    for (let itemClass of this.classL) {
      block.classList.toggle(itemClass);
    }

    return block;
  }
}

// a partir de un objeto crear el bloque
const testBlock = new htmlElement("a",["class1", "class2"],null,"sample text");

// function para crear multiples bloques
function multiBlock(bType, arrClass, atributes, arrtext) {

}

// funcion para crear bloques con clases, texto y atributos
function createBlock(bType, arrClass, atributes, text) {
  const block = document.createElement(bType);
  // introducir texto
  if (text) {
    const blockText = document.createTextNode(text);
    block.appendChild(blockText);
  }
  // atributos del elemento
  if (atributes) {
    for (const key in atributes) {
      block.setAttribute(key, `${atributes[key]}`);
    }
  }
  // classes del elemento
  for (const itemClass of arrClass) {
    block.classList.toggle(itemClass);
  }

  return block;
}
window.onload = () => {
  document.body.appendChild(testBlock.createBlock());
  console.log(testBlock);
};
