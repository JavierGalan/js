console.log("hello world");
const fetch = require("node-fetch");

const fetchPokemoData = async ()=>{
    const endPoint = `https://pokeapi.co/api/v2/pokemon?limit=150&offset=0`;
    const pokemonDataRequest = await fetch(endPoint).catch((err)=>console.log(err));
    if (pokemonDataRequest.status !== 200) {
        return Promise.reject
    }
    const pokemonDataAnswer = await pokemonDataRequest.json();
    return pokemonDataAnswer;
}

console.log(fetchPokemoData());
fetchPokemoData().then((pokemon)=>console.log(pokemon));