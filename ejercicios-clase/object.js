const person= {
    name:'Francisco Javier',
    lastName: 'Galán',
    languajes:{
        main:'JavaScript',
        secondary:'JS',
    },
}

// const {name, lastName} = person;
// console.log(name);
// console.log(lastName);

const { main, lastName, languajes } = person;
console.log(languajes.main)

let xmen = ["Ciclops", "Beast", "Angel", "Marvel-girl"];
let newXmen = ["Wolverine", "NightCrawler", "Storm"];

// Antes se usaba el concat y ahora ...
let myMutants = [...xmen, ...newXmen];

// Se puede usar también para copiar un array
let xmenCopy = [...xmen];

// se usa para jugar con tu array sin modificarlo
let [lastMutant] = [...xmen].reverse();

// No se ha modificado
console.log(xmen);
console.log("ultimo mutante hijodeputa",lastMutant);