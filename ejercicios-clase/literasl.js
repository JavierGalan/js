/**
* Pequeño ejercicio. let y const / string literals;
*
* Crea una variable llamada saludo e imprimela por consola. Luego modifica su valor y vuelve a imprimirla.
* Esto tiene que imprimirse bien.
*
* Crea una variable llamada saludo2 e imprimela por consola. Luego modifica su valor y vuelve a imprimirla.
* Esta asignación no se debe poder hacer.
*
* const persona = {
* }
*
* Imprime por consola usando el objeto persona: 'Tunombre Tuapellido';
*
* Imprime por consola: 'Hola me llamo
* Tunombre
* Tuapellido y estoy programando'
*
*
* Imprime por consola si está abierto: 'Bienvenido, estamos abiertos';
*
*/
let saludo = "saludo";

const saludo2 = "saludo2";
console.log(saludo);
console.log(saludo2);

const isOpen = false;

const person = {
  name: "juan",
  firstName: "juanez",
};

console.log(`Hola me llamo
${person.name}
${person.firstName} y estoy programando`);

console.log(`Esta abierto? ${isOpen ? 'si.' : 'no.'}`);