/**
 * Crea una función llamada elegirEquipo y recibe como argumentos los nombres de 3 jugadores de la selección Española. -> elegirEquipo('Iker Casillas', 'Morata', 'Sergio Ramos');
 * 
 * Esa función imprimirá un console.log que diga: "Tus jugadores elegidos son: xxxx yyyy nnnnn"
 * 
 * elegirEquipo retorna un array con el nombre de esos 3 jugadores y guárdalo en una variable.
 * 
 * Crea otra función que itere sobre ese array con tus jugadores.
 * 
 * En cada iteración pinta un console.log que diga: "Soy el jugador xxxxx con número Y" (para el Y puedes usar el index);
 * 
 * Si durante el bucle el nombre de jugador coincide con "Morata" -> imprime "Vale, ya me voy".
 * 
 * -----------
 * 
 * Bien, segunda parte. Modifica tu código para que tenga las siguientes funcionalidades.
 * 
 * Ahora, en lugar del nombre de cada jugador enviarás un objeto por cada jugador.
 * 
 * var playersList = [
 *      {name: 'Casillas', dorsal: 24 },
 *      {name: 'Ramos', dorsal: 54 },
 *      {name: 'Morata', dorsal: 19 },
 * ]
 * 
 * La función chooseTeam (elegirEquipo) recibirá un array de objetos(playerList).
 * Devolverá lo mismo que devuelve ahora.
 * 
 * La función printTeam recibirá el array de objetos (playerList) e imprimirá el numero correcto del jugador.
 * 
 * Ahora, en lugar de comparar "Morata", tendremos definido un array con Nombres. Si coincide uno de esos nombres
 * imprimirá 'Vale, ya me voy';
 * 
 * var expelled = ['Morata', 'Ronaldo', 'Maradona', 'Casillas'];
 * 
 * Ahora, printTeam tendrá que retornar un nuevo array de objetos con el siguiente esquema
 * 
 * [
 *      {name: 'Casillas', dorsal: 23, isExpelled: true, isGoodPlayer: true}
 * ]
 * 
 * Si el jugador tiene su nombre en el array de expelled, la variable isExpelled será true, de lo contrario false.
 * isGoodPlayer: Si el dorsal es número par será true. Si es impar será false.
 * 
 * 
 * Por último, guarda en una variable el return de printTeam y sácalo por consola.
 */

function chooseTeam(arrayList) {
    var arrPlayers = [];
    for (let i = 0; i < list.length; i++) {
        console.log(`Nombre del jugador${arrayList[i].name}`);
    }
};


function printTeam(daniel) { // sé que voy a esperar un array de strings.

}

var expelled = ['Morata', 'Ronaldo', 'Maradona', 'Casillas'];

var playersList = [
    { name: 'Casillas', dorsal: 24 },
    { name: 'Ramos', dorsal: 54 },
    { name: 'Morata', dorsal: 19 },
    { name: 'Zidane', dorsal: 45 },
    { name: 'Pedri', dorsal: 15 },
    { name: 'Güiza', dorsal: 11 },
]
chooseTeam(playersList = [
    { name: 'Casillas', dorsal: 24 },
    { name: 'Ramos', dorsal: 54 },
    { name: 'Morata', dorsal: 19 },
    { name: 'Zidane', dorsal: 45 },
    { name: 'Pedri', dorsal: 15 },
    { name: 'Güiza', dorsal: 11 },
]);
