// Queremos comprobar las identidades secretas de algunos de nuestros super-heroes favoritos. Cuando introducimos su
//  nombre en clave nos aparecerá un mensaje en consola con el nombre real.Para ello tenemos los siguientes heroes:

// Ironman - Tony Stark
// Hulk - Bruce Banner
// Spiderman - Peter Parker
// Capitan America - Steve Rogers
// Lobezno - Logan
// Tormenta - Ororo
// Fenix - Jean Grey
var heroes = ['IronMan', 'Hulk', 'Spiderman', 'Capitan America', 'Lobezno', 'Tormenta', 'Fenix', 'test'];
var names = ['Tony Stark', 'Bruce Banner', 'Peter Parker', 'Steve Rogers', 'Logan', 'Ororo', 'Jean Grey'];

var heroesNames = [];

var myHeroe = {
    heroe: '',
    name: ''
}

for (let i = 0; i < heroes.length; i++) {
    if (heroes[i] && names[i]) {
        myHeroe.heroe = heroes[i];
        myHeroe.name = names[i];
        heroesNames.push(myHeroe);
    }
    myHeroe = {};
}
console.log('*****bucles***** \n', heroesNames);

console.log('*****El interruptor*****');
var animes = ['My heroes academia', 'Attack on Titans', 'One Piece'];
for (let i = 0; i < animes.length; i++) {
    switch (animes[i]) {
        case 'My heroes academia':
            console.log(animes[i], 'Muy Otako, duchate bien');
            break;
        case 'Attack on Titans':
            console.log(animes[i], 'No esta mal pero es Otako, duchate');
            break;
        case 'One Piece':
            console.log(animes[i], 'Tremendamente Otako, una sola ducha no es suficiente');
            break;
        default:
            console.log('Get a live');
            break;
    }    
}