    console.log("Ejemplos con arrow");

/**
 * ARROW FUNCTIONS excercises -> REFACTORIZAME ESTA PRIMO
 * 
 * 1. Crea una función que reciba un nombre y un apellido. Tiene que retornar el nombre junto al apellido.
 * 
 * 2. Crea una función dadas un nombre y un apellido, retorne un objeto con las propiedades 'name' y 'lastName'
 * 
 * 3. Crea una función que retorne otra función. La primera recibe un nombre, la función retornada recibe un apellido. La 
 * ejecutición de esta segunda función tiene que imprimir 'Hola, me llamo *nombre* *apellido*. Un placer'.
 * 
 * Refactoriza la siguiente función:
 * 
 * function createMultiple(items, builder, container) {
        items.forEach(function(item) {
            container.appendChild(builder(item));
        });
    };
 */
    
    const data = (name, firstName) => `${name} ${firstName}`;
    
    const object = (name, firstName) => ({name: name, firstName:firstName});
    console.log(object("Kerri", "Caberga"));

    const la3 = ({name})=>{}
    const la32 = ({lastName})=>{}
    console.log(la3("Kerri", "Caberga"))
