// paso1 instalar fetch
const fetch = require("node-fetch");

const fetchPokemonDetail = async (pokeNameId) => {
  const endPoint = `https://pokeapi.co/api/v2/pokemon/${pokeNameId}`;
  const pokemonDetailRequest = await fetch(endPoint).catch((err) =>
    console.log(err)
  );

  // manejo del error, si no es 200(200 = todo bien)
  if (pokemonDetailRequest.status !== 200) {
    return Promise.reject;
  }

  const pokemonDetailAnswer = await pokemonDetailRequest.json();
  // console.log("200", pokemonDetailAnswer);

  // let pokemonDataTransform = {
  //   id: pokemonDetailAnswer.id,
  //   name: pokemonDetailAnswer.name,
  //   order: pokemonDetailAnswer.order,
  //   type: pokemonDetailAnswer.types.map((element)=>element.type.name).join(","),
  //   attacks: pokemonDetailAnswer.abilities.map((abilitie)=>abilitie.ability.name).join(", "),
  // };
  //   desestructurado
  const { id, name, order, types, abilities } = pokemonDetailAnswer;
  let pokemonDataTransform = {
    id,
    name,
    order,
    types: types.map((element) => element.type.name).join(", "),
    attacks: abilities.map((abilitie) => abilitie.ability.name).join(", "),
  };
  return pokemonDataTransform;
};

// fetchPokemonDetail('haunter').then((pokemon) => {console.log(pokemon);});

const fetchPokemonList = async () => {
  const endPoint = `https://pokeapi.co/api/v2/pokemon?limit=150&offset=0`;
  const pokemonListRequest = await fetch(endPoint).catch((err) =>
    console.log(err)
  );
  if (pokemonListRequest.status !== 200) {
    return Promise.reject;
  }
  const pokemonListAnswer = await pokemonListRequest.json();
  // console.log("200", pokemonDetailAnswer);
  let pokemonListDataTransform = {
    // name: pokemonListAnswer.results.map((pokemon) => pokemon.name),
    name: pokemonListAnswer.results.map((pokemon) => {
      return pokemon.name;
    }),
  };
  return pokemonListDataTransform;
};
// fetchPokemonList().then((pokemons)=>{console.log(pokemons)}).catch((err) =>console.log(err));

const fetchPokemonRandom = async () => {
  const pokemonRandom = Math.floor(Math.random() * (150 - 0) + 0);
  const endPoint = `https://pokeapi.co/api/v2/pokemon/${pokemonRandom}/`;
  const pokemonDetailRequest = await fetch(endPoint).catch((err) =>console.log(err));
  if (pokemonDetailRequest.status !== 200) {
    return Promise.reject;
  }
  const pokemonDetailAnswer = await pokemonDetailRequest.json();
  const { id, name, order, types, abilities } = pokemonDetailAnswer;
  let pokemonDataTransform = {
    id,
    name,
    order,
    types: types.map((element) => element.type.name).join(", "),
    attacks: abilities.map((abilitie) => abilitie.ability.name).join(", "),
  };
  return pokemonDataTransform;
}
fetchPokemonRandom().then((pokemon)=>console.log(pokemon));